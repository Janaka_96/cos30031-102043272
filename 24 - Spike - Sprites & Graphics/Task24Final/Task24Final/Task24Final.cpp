// Task 24.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include "pch.h"

//Using SDL, SDL_image, standard math, and strings
#include <SDL.h>
#include <SDL_image.h>
#include <stdio.h>
#include <windows.h>
#include <string>

//Screen dimension constants
const int SCREEN_WIDTH = 800;
const int SCREEN_HEIGHT = 600;

bool choice = true;
class Texture
{
public:

	Texture();

	~Texture();

	bool loadFromFile(std::string path);
	void free();

	void render(int x, int y, SDL_Rect* clip = NULL);
	int getWidth();
	int getHeight();

private:
	SDL_Texture* Texture1;
	SDL_Texture* Texture2;
	int Width;
	int Height;

};

bool init();

bool loadMedia();

void close();

SDL_Window* Window = NULL;

SDL_Renderer* Renderer = NULL;

SDL_Rect SpriteClips[3];
SDL_Rect bg;

Texture SpriteSheetTexture;
Texture bgimage;


Texture::Texture()
{

	Texture1 = NULL;
	Texture2 = NULL;
	Width = 0;
	Height = 0;

}

Texture::~Texture()
{

	free();
}

bool Texture::loadFromFile(std::string path)
{

	free();

	SDL_Texture* newTexture = NULL;

	SDL_Surface* loadedSurface = IMG_Load(path.c_str());
	if (loadedSurface == NULL)
	{
		printf("Unable to load image %s! SDL_image Error: %s\n", path.c_str(), IMG_GetError());
	}
	else
	{
		SDL_SetColorKey(loadedSurface, SDL_TRUE, SDL_MapRGB(loadedSurface->format, 0, 0xFF, 0xFF));


		newTexture = SDL_CreateTextureFromSurface(Renderer, loadedSurface);
		if (newTexture == NULL)
		{
			printf("Unable to create texture from %s! SDL Error: %s\n", path.c_str(), SDL_GetError());
		}
		else
		{

			Width = loadedSurface->w;
			Height = loadedSurface->h;
		}

		SDL_FreeSurface(loadedSurface);
	}


	Texture1 = newTexture;
	return Texture1 != NULL;
}


void Texture::free()
{
	if (Texture1 != NULL)
	{
		SDL_DestroyTexture(Texture1);
		Texture1 = NULL;
		Width = 0;
		Height = 0;
		SDL_DestroyTexture(Texture2);
		Texture2 = NULL;

	}
}

void Texture::render(int x, int y, SDL_Rect* clip)
{

	SDL_Rect renderQuad = { x, y, Width, Height };

	if (clip != NULL)
	{
		renderQuad.w = clip->w;
		renderQuad.h = clip->h;
	}


	SDL_RenderCopy(Renderer, Texture1, clip, &renderQuad);
}




int Texture::getWidth()
{
	return Width;
}

int Texture::getHeight()
{
	return Height;
}

bool init()
{
	bool success = true;
	if (SDL_Init(SDL_INIT_VIDEO) < 0)
	{
		printf("SDL could not initialize! SDL Error: %s\n", SDL_GetError());
		success = false;
	}
	else {
		if (!SDL_SetHint(SDL_HINT_RENDER_SCALE_QUALITY, "1"))
		{
			printf("Warning: Linear texture filtering not enabled!");
		}

		Window = SDL_CreateWindow("Task 24 - Sprites and Graphics", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, SCREEN_WIDTH, SCREEN_HEIGHT, SDL_WINDOW_SHOWN);
		if (Window == NULL)
		{
			printf("Window could not be created! SDL Error: %s\n", SDL_GetError());
			success = false;
		}
		else
		{
			Renderer = SDL_CreateRenderer(Window, -1, SDL_RENDERER_ACCELERATED);
			if (Renderer == NULL)
			{
				printf("Renderer could not be created! SDL Error: %s\n", SDL_GetError());
				success = false;
			}
			else
			{
				SDL_SetRenderDrawColor(Renderer, 0xFF, 0xFF, 0xFF, 0xFF);
				int imgFlags = IMG_INIT_PNG;
				if (!(IMG_Init(imgFlags) & imgFlags))
				{
					printf("SDL_image could not initialize! SDL_mage Error: %s\n", IMG_GetError());
					success = false;
				}
			}
		}
	}

	return success;
}

bool loadMedia()
{
	bool success = true;




	if (!SpriteSheetTexture.loadFromFile("./dots.png"))
	{
		printf("Failed to load sprite sheet texture!\n");
		success = false;
	}
	if (!bgimage.loadFromFile("./image.jpg"))
	{
		printf("Failed to load sprite sheet texture!\n");
		success = false;
	}



	return success;
}

void close()
{

	SpriteSheetTexture.free();


	SDL_DestroyRenderer(Renderer);
	SDL_DestroyWindow(Window);
	Window = NULL;
	Renderer = NULL;

	IMG_Quit();
	SDL_Quit();
}

int main(int argc, char* args[])
{


	if (!init())
	{
		printf("Failed to initialize!\n");
	}
	else
	{

		if (!loadMedia())
		{
			printf("Failed to load media!\n");
		}
		else
		{
			bool quit = false;

			SDL_Event e;


			while (!quit)
			{


				while (SDL_PollEvent(&e) != 0)
				{
					if (e.type == SDL_KEYDOWN) {
						switch (e.key.keysym.sym)
						{
						case SDLK_1:
							if (choice) {
								SpriteClips[0].x = 0;
								SpriteClips[0].y = 0;
								SpriteClips[0].w = 100;
								SpriteClips[0].h = 100;
								choice = false;
							}
							else {
								SpriteClips[0].x = NULL;
								SpriteClips[0].y = NULL;
								SpriteClips[0].w = NULL;
								SpriteClips[0].h = NULL;
								choice = true;
							}

							break;

						case SDLK_2:
							if (choice) {
								SpriteClips[1].x = 100;
								SpriteClips[1].y = 0;
								SpriteClips[1].w = 100;
								SpriteClips[1].h = 100;
								choice = false;
							}
							else {
								SpriteClips[1].x = NULL;
								SpriteClips[1].y = NULL;
								SpriteClips[1].w = NULL;
								SpriteClips[1].h = NULL;
								choice = true;
							}

							break;

						case SDLK_3:
							if (choice) {
								SpriteClips[2].x = 0;
								SpriteClips[2].y = 100;
								SpriteClips[2].w = 100;
								SpriteClips[2].h = 100;
								choice = false;
							}
							else {
								SpriteClips[2].x = NULL;
								SpriteClips[2].y = NULL;
								SpriteClips[2].w = NULL;
								SpriteClips[2].h = NULL;
								choice = true;
							}

							break;

						case SDLK_0:
							if (choice) {
								bg.x = 0;
								bg.y = 0;
								bg.w = 800;
								bg.h = 600;
								choice = false;
							}
							else {
								bg.x = NULL;
								bg.y = NULL;
								bg.w = NULL;
								bg.h = NULL;
								choice = true;
							}

							break;
						}
					}
					else if (e.type == SDL_QUIT)
					{
						quit = true;
					}

				}

				SDL_SetRenderDrawColor(Renderer, 0xFF, 0xFF, 0xFF, 0xFF);

				SDL_RenderClear(Renderer);

				bgimage.render(0, 0, &bg);


				SpriteSheetTexture.render(0, 0, &SpriteClips[0]);

				SpriteSheetTexture.render(SCREEN_WIDTH - SpriteClips[1].w, 0, &SpriteClips[1]);


				SpriteSheetTexture.render(0, SCREEN_HEIGHT - SpriteClips[2].h, &SpriteClips[2]);


				SDL_RenderPresent(Renderer);


			}
		}
	}


	close();

	return 0;
}