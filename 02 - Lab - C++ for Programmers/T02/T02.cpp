

#include "stdafx.h"
#include "classDemo.h"
#include <iostream>
#include <vector>
#include <string>


using namespace std;

void AcceptValuesReturnNothing(int a, int b){

	cout << "First Value :" << a << endl;
	cout << "Second Value :" << b << endl;


}

int AlterValue(int val){
	val = val + 100;
	return val;
}

void loop(){
	
	for (int i = 0; i < 20; i++){
		i = i + 1;
		cout << i << " ";
		
	}
	cout << endl;
}

void Array(){
	int array[5];
	for (int i = 0; i < 5; i++){
		array[i] = rand();
		cout << array[i] << " ";

	}
	cout << endl;
}

const vector<string> split(const string& b, const char& c)
{
	string buff{ "" };
	vector<string> v;

	for (auto a : b)
	{
		if (a != c) buff += a; else
			if (a == c && buff != "") { v.push_back(buff); buff = ""; }
	}
	if (buff != "") v.push_back(buff);

	return v;
}


int main()
{
	int a = 10;
	int b = 20;
	int val = 30;
	int value;
	int *pointer;
	


	AcceptValuesReturnNothing(a, b);

	cout << "Altered Vale : " << AlterValue(val) << endl;

	pointer = &value;
	*pointer = 40;
	cout << "Using Pointers the value is : " << value << endl;

	cout << "The Odd Number between 1 -20 :"<< endl;
	loop();
	cout << endl;

	cout << "Array with random values : " << endl;
	Array();
	cout << endl;
	cout << "Using String Split :" << endl;
	string str{ "This,will,be,split,from,commas" };
	vector<string> v{ split(str, ',') };
	for (auto a : v) cout << a << endl;

	cout << "Using a simple class :" << endl;
	classDemo demo;
	demo.add();
	

	return 0;
}

