// Task 10.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"

#include <iostream>
#include <vector>
#include <Windows.h>
#include <string>

using namespace std;

void Red()
{
	SetConsoleTextAttribute
		(GetStdHandle(STD_OUTPUT_HANDLE), FOREGROUND_RED | FOREGROUND_INTENSITY);
} //Intensive red console text color.

void Green()
{
	SetConsoleTextAttribute
		(GetStdHandle(STD_OUTPUT_HANDLE), FOREGROUND_GREEN | FOREGROUND_INTENSITY);
} //Intensive green console text color.

void Normal()
{
	SetConsoleTextAttribute
		(GetStdHandle(STD_OUTPUT_HANDLE), FOREGROUND_GREEN | FOREGROUND_RED | FOREGROUND_BLUE);
} 

struct Item{
	string name; 
	int bodypart; 

};

int main()
{
	cout << " +-+-+-+-+-+-+-+-+-+" << endl;
	cout << " |I|n|v|e|n|t|o|r|y|" << endl;
	cout << " +-+-+-+-+-+-+-+-+-+" << endl;
	cout << endl;

	cout << "Type Equipment to check for equipped Items" << endl;
	cout << "Type Inventory to check your inventory" << endl;
	cout << "Type Equip to equip an Item" << endl;
	Item Sword{
		"Sword",
		3 
	};

	Item Armor{
		"Steel Plate",
		2
	};

	Item LegArmor{
		"Knee Guard",
		4
	};

	Item Shoes{
		"Steel Boots",
		5
	};
	Item Helmet{
		"Steel Helmet",
		1
	};

	string input; // input is for player commands.
	vector<string> Equipment = { "<Empty>", "<Empty>", "<Empty>", "<Empty>", "<Empty>", "<Empty>" }; //Current equipment.
	vector<string> Inventory = { Sword.name, Armor.name, LegArmor.name, Shoes.name, Helmet.name}; //Player Inventory.
	string InventorySlots[] = { "Head", "Neck", "Torso", "Hands", "Legs", "Feet" }; //Player parts where items can be equiped.

	while (true){
		cin >> input;
		if (input == "equipment" | input == "EQUIPMENT" | input == "Equipment"){
			for (int i = 0; i < 6; i++){
				Normal();
				cout << InventorySlots[i];
				if (Equipment[i] == "<Empty>"){
					Red();
					cout << " " << Equipment[i] << endl << endl;
				}
				else if (Equipment[i] != "<Empty>"){
					Green();
					cout << " " << Equipment[i] << endl << endl;
				}
				Normal();
			}
		}

		if (input == "equip" | input == "EQUIP" | input == "Equip"){
			cout << "Select the items you want to equip from the list" << endl;
			for (int i = 0; i < Inventory.size(); i++){
				cout << i+1 <<" - " + Inventory[i] << endl;
			}

			cout << "Select 1-5:> ";
			char equip;
			cin >> equip;
			if (equip == '1'){
				Equipment[3] = "Sword";
				cout << "Successfully equiped!" << endl;
			}
			else if (equip == '2'){
				Equipment[2] = "Steel Plate";
				cout << "Successfully equiped!" << endl;
			}
			else if (equip == '3'){
				Equipment[4] = "Knee Guard";
				cout << "Successfully equiped!" << endl;
			}
			else if (equip == '4'){
				Equipment[5] = "Steel Boots";
				cout << "Successfully equiped!" << endl;
			}
			else if (equip == '5'){
				Equipment[0] = "Helmet";
				cout << "Successfully equiped!" << endl;
			}
			
		}

		if (input == "inventory"){
			for (int i = 0; i < Inventory.size(); i++){
				cout << "________________________________" << endl;
				cout << "|                               |" << endl;
			    cout << "|     " << Inventory[i] <<"     |"<< endl;
				cout << "|_______________________________|" << endl;
			}
		} //else if (input == "inventory")

	}
	system("PAUSE"); // or 'cin.get()'
	return 0;
}