

#include "stdafx.h"
#include <iostream>
#include <string>

using namespace std;

const int MapWidth = 8;
const int MapHeight = 8;

// These make our lives easier if we ever decide to change the symbols:
const char Player = 'S';
const char Death = 'D';
const char Gold = 'G';
const char Road = ' ';
const char Wall = '#';

enum Keys
{
	East = 'E',
	West = 'W',
	North = 'N',
	South = 'S',
	Quit = 'Q'
};

enum GameState
{
	Dead,
	FoundGold,
	Continue,
	QuitGame,
	GoBack
};

void display(const char map[MapWidth][MapHeight])
{
	for (int y = 0; y < MapHeight; ++y)
	{
		for (int x = 0; x < MapWidth; ++x)
		{
			cout << " ";
			cout << map[x][y];
		}
		cout << "\n";
	}
}

void newMap(char map[MapWidth][MapHeight])
{
	for (int y = 0; y < MapHeight; ++y)
	{
		for (int x = 0; x < MapWidth; ++x)
		{
		map[x][y] = ' ';
			
			
			
	}
		
	}
}

void makeMap(char map[MapWidth][MapHeight])
{

	map[2][7] = Player;

	map[3][1] = Death;
	map[5][1] = Death;
	map[6][3] = Death;


	map[1][1] = Gold;

	map[0][0] = Wall;
	map[0][1] = Wall;
	map[0][2] = Wall;
	map[0][3] = Wall;
	map[0][4] = Wall;
	map[0][5] = Wall;
	map[0][6] = Wall;
	map[0][7] = Wall;
	map[0][0] = Wall;
	map[1][0] = Wall;
	map[2][0] = Wall;
	map[3][0] = Wall;
	map[4][0] = Wall;
	map[5][0] = Wall;
	map[6][0] = Wall;
	map[7][0] = Wall;
	map[4][1] = Wall;
	map[7][1] = Wall;
	map[4][2] = Wall;
	map[7][2] = Wall;
	map[1][3] = Wall;
	map[2][3] = Wall;
	map[4][3] = Wall;
	map[7][3] = Wall;
	map[4][4] = Wall;
	map[7][4] = Wall;
	map[2][5] = Wall;
	map[3][5] = Wall;
	map[4][5] = Wall;
	map[5][5] = Wall;
	map[7][5] = Wall;
	map[7][6] = Wall;
	map[1][7] = Wall;
	map[3][7] = Wall;
	map[4][7] = Wall;
	map[5][7] = Wall;
	map[6][7] = Wall;
	map[7][7] = Wall;

}

GameState move(char map[MapWidth][MapHeight],
	int & currentX, int & currentY,
	int desiredX, int desiredY, char key)
{

	if (desiredX < 0) { desiredX = 0; }
	if (desiredY < 0) { desiredY = 0; }
	if (desiredX >= MapWidth)  { desiredX = MapWidth - 1; }
	if (desiredY >= MapHeight) { desiredY = MapHeight - 1; }
	
	if (key == East &&  map[desiredX][desiredY] == Wall) { desiredX = desiredX - 1; }
	if (key == West &&  map[desiredX][desiredY] == Wall) { desiredX = desiredX + 1; }
	if (key == North &&  map[desiredX][desiredY] == Wall) { desiredY = desiredY + 1; }
	if (key == South &&  map[desiredX][desiredY] == Wall) { desiredY = desiredY - 1; }


	GameState newState;


	if (map[desiredX][desiredY] == Death)
	{
		cout << endl;
		cout << "Dead End" << endl << "YOU HAVE DIED!" << endl << "Thanks for playing. Maybe next time.\n";
		cout << endl;

		newState = Dead;
	}
	else if (map[desiredX][desiredY] == Gold)
	{
		cout << endl;
		cout << "Congrats You've found the Gold!" << endl << "YOU WIN!" << endl << "Thanks for playing.\n";
		cout << endl;

		newState = FoundGold;

	}
	else{
		newState = Continue;
	}


	map[currentX][currentY] = Road;


	map[desiredX][desiredY] = Player;
	
	if (map[desiredX][desiredY] != Wall && map[currentX][currentY] != Wall){
		currentX = desiredX;
		currentY = desiredY;

	}

		
	
	return newState;
}

int main()
{
	cout << "  =========================================================================== " << endl;
	cout << "   .d8888b.          d8b      888 888       888                  888      888 " << endl;
	cout << "  d88P  Y88b         Y8P      888 888   o   888                  888      888  " << endl;
	cout << "  888    888                  888 888  d8b  888                  888      888  " << endl;
	cout << "  888        888d888 888  .d88888 888 d888b 888  .d88b.  888d888 888  .d88888  " << endl;
	cout << "  888  88888 888P'   888 d88' 888 888d88888b888 d88''88b 888P'   888 d88' 888  " << endl;
	cout << "  888    888 888     888 888  888 88888P Y88888 888  888 888     888 888  888  " << endl;
	cout << "  Y88b  d88P 888     888 Y88b 888 8888P   Y8888 Y88..88P 888     888 Y88b 888  " << endl;
	cout << "   'Y8888P88 888     888  'Y88888 888P     Y888  'Y88P'  888     888  'Y88888  " << endl;
	cout << "  ============================================================================ " << endl;
	cout << "  ============================================================================ " << endl;
	cout << endl;

	cout << "	Welcome to GridWorld : Quantised Excitement.Fate is waiting for You!" << endl;
	cout << "	Valid commands : N, S, E and W for direction.Q to quit the game. " << endl;

	cout << endl;
	cout << endl;
	cout << "	Shown below is the map for the game 						" << endl;
	cout << "	G and D shows gold and death locations						" << endl;
	cout << "	S is the starting position							" << endl;
	cout << endl;
	cout << endl;

	int X = 2;
	int Y = 7;
	GameState gameState = Continue;
	char gameMap[MapWidth][MapHeight];
	newMap(gameMap);
	makeMap(gameMap);
	display(gameMap);

	while (gameState == Continue )
	{

		char key;
		cout << "Move your player: >";
		cin >> key;
		
		if (key == East)
			{
				gameState = move(gameMap, X, Y, X + 1, Y, key);
			}
		else if (key == West)
			{
				gameState = move(gameMap, X, Y, X - 1, Y, key);
			}
		else if (key == North)
			{
				gameState = move(gameMap, X, Y, X, Y - 1, key);
			}
		else if (key == South)
			{
				gameState = move(gameMap, X, Y, X, Y + 1, key);
			}
			else if (key == Quit)
			{
				gameState = QuitGame;
			}
			else
			{
				cout << "Invalid Key, use N,S,W,E to move up, down, left, right. Q to quit.\n";
			}
		

		display(gameMap);
	
	}
	
	return 0;
}

