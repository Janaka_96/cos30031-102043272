// Task 23.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include "pch.h"


#include <SDL.h>
#include <SDL_image.h>
#include <SDL_mixer.h>
#include <stdio.h>
#include <string>


const int SCREEN_WIDTH = 640;
const int SCREEN_HEIGHT = 480;


bool initialize();


void close();


SDL_Window* Window = NULL;

SDL_Renderer* Renderer = NULL;



bool initialize()
{

	bool state = true;

	
	if (SDL_Init(SDL_INIT_EVERYTHING) < 0)
	{
		printf("SDL could not initialize! SDL Error: %s\n", SDL_GetError());
		state = false;
	}
	else
	{
		Window = SDL_CreateWindow("Task 23 Sound Board", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, SCREEN_WIDTH, SCREEN_HEIGHT, SDL_WINDOW_SHOWN);
		Renderer = SDL_CreateRenderer(Window, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
				
		if (Mix_OpenAudio(44100, MIX_DEFAULT_FORMAT, 2, 2048) < 0)
				{
					printf("SDL_mixer could not initialize! SDL_mixer Error: %s\n", Mix_GetError());
					state = false;
				}
	}

	return state;
}

void close()
{
	Mix_FreeChunk(Mix_LoadWAV("./Ammata1.wav"));
	Mix_FreeChunk(Mix_LoadWAV("./Huka.wav"));
	Mix_FreeChunk(Mix_LoadWAV("./Hutht.wav"));
	Mix_FreeMusic(Mix_LoadMUS("./fade.wav"));
	SDL_DestroyRenderer(Renderer);
	SDL_DestroyWindow(Window);
	Window = NULL;
	Renderer = NULL;
	Mix_Quit();
	IMG_Quit();
	SDL_Quit();
}

int main(int argc, char* args[])
{
	if (!initialize())
	{
		printf("Failed to initialize!\n");
	}
	else
	{
		bool quit = false;
		SDL_Event e;

		while (!quit)
		{
		
			while (SDL_PollEvent(&e) != 0)
			{
			
				if (e.type == SDL_QUIT)
				{
					quit = true;
				}
				
				else if (e.type == SDL_KEYDOWN)
				{
					switch (e.key.keysym.sym)
					{
					
					case SDLK_1:
						Mix_PlayChannel(-1, Mix_LoadWAV("./Ammata1.wav"), 0);
						break;
					
					case SDLK_2:
						Mix_PlayChannel(-1, Mix_LoadWAV("./Huka.wav"), 0);
						break;

						
					case SDLK_3:
						Mix_PlayChannel(-1, Mix_LoadWAV("./Hutht.wav"), 0);
						break;

					case SDLK_0:
						
						if (Mix_PlayingMusic() == 0)
						{
							
							Mix_PlayMusic(Mix_LoadMUS("./fade.wav"), -1);
						}
					
						else
						{
							if (Mix_PausedMusic() == 1)
							{
								Mix_ResumeMusic();
							}
							else
							{
								Mix_PauseMusic();
							}
						}
						break;
					}



				
				}
			}
		}

		
		close();

		return 0;
	}
}