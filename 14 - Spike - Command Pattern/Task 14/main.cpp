#include <iostream>
#include <string>
#include <vector>
#include <conio.h>
#include "globals.h"
#include "world.h"
#include <windows.h>
#include <fstream>

using namespace std;



using namespace std;

enum GameStates{
	State_Null,
	MainMenu,
	About,
	SelectAdventure,
	Help,
	Gameplay1,
	NewHighScore,
	HallofFame,
	Exit


};

//Game state base class
class GameState
{
public:
	virtual void handle_events() = 0;
	virtual ~GameState(){};
};

class mainMenu : public GameState{

public:

	mainMenu();
	~mainMenu();

	void handle_events();

};

class selectAdventure : public GameState{
public:
	selectAdventure();
	~selectAdventure();

	void handle_events();

};

class help : public GameState{
public:
	help();
	~help();

	void handle_events();

};

class about : public GameState{
public:
	about();
	~about();

	void handle_events();

};

class gameplay1 : public GameState{
public:
	gameplay1();
	~gameplay1();

	void handle_events();

};

class newHighScore : public GameState{
public:
	newHighScore();
	~newHighScore();

	void handle_events();

};

class hallofFame : public GameState{
public:
	hallofFame();
	~hallofFame();

	void handle_events();

};


void set_next_state(int newState);

void change_state();

GameState *currentState = NULL;

int stateID = State_Null;
int nextState = State_Null;

char choice;

mainMenu::mainMenu(){
	cout << "Zorkish :: Main Menu" << endl;
	cout << "--------------------------------------------------------" << endl;
	cout << endl;
	cout << endl;
	cout << "Welcome to Zorkish Adventures" << endl;
	cout << endl;
	cout << "     1. Select Adventure and Play" << endl;
	cout << "     2. Hall of Fame" << endl;
	cout << "     3. Help" << endl;
	cout << "     4. About" << endl;
	cout << "     5. Quit" << endl;
	cout << endl;
	cout << "Select 1-5:> ";
	cin >> choice;
}
mainMenu::~mainMenu(){

}
void mainMenu::handle_events(){
	if (choice == '5'){
		set_next_state(State_Null);
	}
	else if (choice == '1'){
		set_next_state(SelectAdventure);
	}
	else if (choice == '2'){
		set_next_state(HallofFame);
	}
	else if (choice == '3'){
		set_next_state(Help);
	}
	else if (choice == '4'){
		set_next_state(About);
	}

}


selectAdventure::selectAdventure(){
	cout << "Zorkish :: Select Adventure" << endl;
	cout << "--------------------------------------------------------" << endl;
	cout << endl;
	cout << endl;
	cout << "Choose your Adventure:" << endl;
	cout << endl;
	cout << "     1. Mountain World" << endl;
	cout << "     2. Water World" << endl;
	cout << "     3. Box World" << endl;

	cout << endl;
	cout << "Select 1-3:> ";
	cin >> choice;
}
selectAdventure::~selectAdventure(){

}

void selectAdventure::handle_events(){
	if (choice == '1'){
		set_next_state(Gameplay1);
	}
	else if (choice == '2'){
		//set_next_state(SelectAdventure);
	}
	else if (choice == '3'){
		//set_next_state(HallofFame);
	}
}

about::about(){
	cout << "Zorkish :: About" << endl;
	cout << "--------------------------------------------------------" << endl;
	cout << endl;
	cout << endl;
	cout << "Written By: Janaka Wimalaweera" << endl;
	cout << endl;
	cout << "Press Escape to Return to Main Menu:> ";;
	cin >> choice;
}

about::~about(){

}

void about::handle_events(){
	if (GetKeyState(VK_ESCAPE)){
		set_next_state(MainMenu);
	}
}

hallofFame::hallofFame(){
	cout << "Zorkish :: Hall of Fame" << endl;
	cout << "--------------------------------------------------------" << endl;
	cout << endl;
	cout << endl;
	cout << "Top 10 Zorkish Adventure Champions:" << endl;
	cout << endl;
	cout << "1.  Janaka" << endl;
	cout << "2.  Janaka" << endl;
	cout << "3.  Janaka" << endl;
	cout << "4.  Janaka" << endl;
	cout << "5.  Janaka" << endl;
	cout << "6.  Janaka" << endl;
	cout << "7.  Janaka" << endl;
	cout << "8.  Janaka" << endl;
	cout << "9.  Janaka" << endl;
	cout << "10. Janaka" << endl;

	cout << endl;
	cout << "Press Escape to Return to Main Menu:> ";

}

hallofFame::~hallofFame(){

}

void hallofFame::handle_events(){
	if (GetKeyState(VK_ESCAPE)){
		set_next_state(MainMenu);
	}
}

help::help(){
	cout << "Zorkish :: Help" << endl;
	cout << "--------------------------------------------------------" << endl;
	cout << endl;
	cout << endl;
	cout << "The Following Commands are Supported:" << endl;
	cout << endl;
	cout << "1.  quit" << endl;
	cout << "2.  High-score" << endl;
	cout << endl;
	cout << "Press Escape to Return to Main Menu:> ";


}
help::~help(){

}

void help::handle_events(){
	if (GetKeyState(VK_ESCAPE)){
		set_next_state(MainMenu);
	}
}

gameplay1::gameplay1(){

	char key;
	string player_input;
	vector<string> args;
	args.reserve(10);

	cout << endl;
	cout << endl;
	cout << "Welcome to Zorkish: Mountain World" << endl;
	cout << "--------------------------------------------------------" << endl;
	ifstream file;
	file.open("./world1.txt");
	string desc;

	while (getline(file,desc)){
		cout << desc << endl;

	
	}
	file.close();

	

	World my_world;

	args.push_back("look");

	while (1)
	{
		if (_kbhit() != 0)
		{
			key = _getch();
			if (key == '\b') // backspace
			{
				if (player_input.length() > 0)
				{
					player_input.pop_back();
					cout << '\b';
					cout << " ";
					cout << '\b';
				}
			}
			else if (key != '\r') // return
			{
				player_input += key;
				cout << key;
			}
			else
				Tokenize(player_input, args);
		}

		if (args.size() > 0 && Same(args[0], "quit"))
			break;

		if (my_world.Tick(args) == false)
			cout << "\nSorry, I am unable to process your command.\n";

		if (args.size() > 0)
		{
			args.clear();
			player_input = "";
			cout << "> ";
		}
	}

}

gameplay1::~gameplay1(){

}

void gameplay1::handle_events(){
	if (GetKeyState(VK_ESCAPE)){
		set_next_state(NewHighScore);
	}
}

newHighScore::newHighScore(){
	cout << "Zorkish :: New High-Score" << endl;
	cout << "--------------------------------------------------------" << endl;
	cout << endl;
	cout << endl;
	cout << "Congratulations!" << endl;
	cout << endl;
	cout << "You have made it to the Zorkih Hall of Fame" << endl;
	cout << endl;
	cout << "Adventure: " << endl;
	cout << "Score: " << endl;
	cout << "Moves: " << endl;
	cout << endl;
	cout << "Please type your name and press Enter: ";
	cin >> choice;
	cout << endl;

}

newHighScore::~newHighScore(){

}

void newHighScore::handle_events(){
	if (GetKeyState(VK_RETURN)){
		set_next_state(MainMenu);
	}
}
void set_next_state(int newState)
{

	if (nextState != Exit)
	{
		nextState = newState;
	}
}

void change_state()
{

	if (nextState != State_Null)
	{

		if (nextState != Exit)
		{
			delete currentState;
		}

		switch (nextState)
		{
		case MainMenu:
			currentState = new mainMenu();
			break;

		case SelectAdventure:
			currentState = new selectAdventure();
			break;

		case About:
			currentState = new about();
			break;

		case HallofFame:
			currentState = new hallofFame();
			break;

		case Help:
			currentState = new help();
			break;

		case Gameplay1:
			currentState = new gameplay1();
			break;

		case NewHighScore:
			currentState = new newHighScore();
			break;

		}

		stateID = nextState;

		nextState = State_Null;
	}
}


int main(){

	stateID = MainMenu;


	currentState = new mainMenu();

	while (stateID != State_Null){
		currentState->handle_events();


		change_state();

	}
}

